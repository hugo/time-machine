#!/usr/bin/env python2

from setuptools import setup

setup(name = "time_machine",
      version = "0.1",
      author = "Hugo Hornquist",
      author_email = "hugo@lysator.liu.se",
      description = "A program for measuring times.",
      license = "MIT",
      url = "https://git.lysator.liu.se/hugo/time-machine",
      packages = ["time_machine"],
      entry_points = {
          "console_scripts": [
              "time-machine = time_machine.machine:main"
          ]
      }
      )
