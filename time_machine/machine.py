#!/usr/bin/env python2

import RPi.GPIO as g
import time
import threading
import sys

import Adafruit_CharLCD as LCD

# For lazer array
# if no bottle present then all are high
# if bottle at least one low

# For regular button
# Down means bottle
# up means no bottle

g.setmode(g.BCM)

pins = [ 17, 27, 22, 10, 9, 11, 5, 6, 13, 19 ]
btn_pin = 4

test = False

lcd_rs = 12
lcd_en = 7
lcd_d4 = 8
lcd_d5 = 25
lcd_d6 = 24
lcd_d7 = 23

lcd_backlight = 2

# Define LCD column and row size for 16x2 LCD.
lcd_columns = 16
lcd_rows = 2

lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, lcd_columns, lcd_rows, lcd_backlight)

risewait = threading.Event()
fallwait = threading.Event()

def callback(pin):
    state = g.input(pin)
    if state == g.LOW:
        print "falling edge"
        fallwait.set()
    else:
        print "rising edge"
        risewait.set()

g.setup(btn_pin, g.IN, pull_up_down=g.PUD_DOWN)
g.setup(3, g.IN, pull_up_down=g.PUD_DOWN)
for pin in pins:
    g.setup(pin, g.IN, pull_up_down=g.PUD_DOWN)
    g.add_event_detect(pin, g.BOTH, callback=callback)

def bottle_check(pins):
    if test:
        return any([g.input(pin) for pin in pins])
    else:
        return not all([g.input(pin) for pin in pins])

class States:
    no_hafv = 0
    hafv_ready = 1
    hafv_active = 2

has_bottle = bottle_check(pins)

state = States.no_hafv
start_time = 0
end_time = 0

def time_ms():
    t = int(time.time() * 1000)
    # print "Time: ", t
    return t

if has_bottle:
    state = States.hafv_ready

def wait_rise():
    risewait.clear()
    risewait.wait()

def wait_fall():
    fallwait.clear()
    fallwait.wait()

# should these be the other way around?

def wait_for_bottle_place():
    return wait_rise() if test else wait_fall()

def wait_for_bottle_remove():
    return wait_fall() if test else wait_rise()

def lcd_info(msg, last_time):
    lcd.clear()
    lcd.home()
    lcd.set_cursor(0, 0)
    lcd.message("Tid: {0} s".format(last_time))
    lcd.set_cursor(0, 1)
    lcd.message(msg)

def wait_btn():
    return g.wait_for_edge(btn_pin, g.RISING, bouncetime=500)
    #g.wait_for_edge(btn_pin, g.RISING)

class Modes:
    button = 0
    flask_raise = 1

def main():
    global state, pins
    last_time = 0
    mode = Modes.button if (sys.argv + [False])[1] else Modes.flask_raise
    while True:
        if state == States.no_hafv:
            print
            print "Inget Hafv"
            lcd_info("Vilar", last_time)
            wait_for_bottle_place()
            state = States.hafv_ready

        elif state == States.hafv_ready:
            print "Hafv redo"
            if mode == Modes.button:
                lcd_info("Starta med knapp", last_time)
                wait_btn()
            elif mode == Modes.flask_raise:
                lcd_info("Starta med lyft", last_time)
                wait_for_bottle_remove()

            state = States.hafv_active
            start_time = time_ms()

        elif state == States.hafv_active:
            print "currently Hafving"
            lcd_info("Klockar", last_time)

            if mode == Mode.button and not bottle_check(pins):
                print "Early start"
                lcd_info("Tjuvstart!", last_time)
                wait_btn()
                state = States.no_hafv
                continue

            print "Raise the bottle"

            # TODO we actually don't need this, but it was
            # here before so it stays until I refactor this.
            if mode == Modes.button:
                wait_for_bottle_remove()
                print "BOTTLE RAISED"

            print "Place back bottle after hafv"

            # Wait for bottle placed is in a loop because if the time
            # is under 1 secound then we want to wait again. This is
            # to prevent a finger or similar from stopping the time.
            while True:
                wait_for_bottle_place()
                print "BOTTLE PLACED"

                end_time = time_ms()
                t = (end_time - start_time) / 1000.0
                if t > 1:
                    # Allow timing to halt
                    break

            print "Hafvtid: {0} s".format(t)
            last_time = t
            lcd_info("Avvakta", last_time)
            state = States.no_hafv
            time.sleep(1)

if __name__ == "__main__":
    main()
